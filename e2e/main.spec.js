'use strict';

describe('The main view', function () {
  var page;

  beforeEach(function () {
    browser.get('/index.html');
    page = require('./main.po');
  });

  it('should give solution for check writer', function () {
    page.checkWriterTab.click();
    page.cwInputEl.sendKeys('123.75');
    page.cwSubmitEl.click();
    expect(page.cwOutputEl.getText()).toBe('one hundred twenty three and 75/100');
  });

});
