/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict';

var MainPage = function () {
  this.tabItems = element.all(by.css('md-tab-item'));
  this.checkWriterTab = this.tabItems.get(0);


  this.cwInputEl = element(by.css('.check-writer input.number'));
  this.cwSubmitEl = element(by.css('.check-writer button.submit'));
  this.cwOutputEl = element(by.css('.check-writer span.output'));
};
module.exports = new MainPage();
