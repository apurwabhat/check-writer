(function () {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .factory('puzzlesQuizService', puzzlesQuizService);

  /** @ngInject */
  function puzzlesQuizService() {

    var service = {
      getCheckWriterSolution: getCheckWriterSolution
    };

    return service;

    function getCheckWriterSolution(numVal) {
      var thousand = ['', 'thousand', 'million', 'billion', 'trillion'];
      var digit = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
      var ten = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
      var twenty = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
      numVal = numVal.toString();
      var x = numVal.indexOf('.');
      if (x == -1) x = numVal.length;
      var n = numVal.split('');
      var str = '';
      var sk = 0;
      for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
          if (n[i] == '1') {
            str += ten[Number(n[i + 1])] + ' ';
            i++;
            sk = 1;
          } else if (n[i] != 0) {
            str += twenty[n[i] - 2] + ' ';
            sk = 1;
          }
        } else if (n[i] != 0) {
          str += digit[n[i]] + ' ';
          if ((x - i) % 3 == 0) str += 'hundred ';
          sk = 1;
        }


        if ((x - i) % 3 == 1) {
          if (sk) str += thousand[(x - i - 1) / 3] + ' ';
          sk = 0;
        }
      }
      if (x != numVal.length) {
        var decVal = numVal.split('.');
        var fracVal = decVal[1];
        if (fracVal == 0) {
          str += 'only'
        } else {
          var z = fracVal.length;
          if (decVal[0] > 0) {
            str += 'and ';
          }
          str += fracVal + '/' + '1';
          for (var l = 0; l < z; l++)
            str += '0';
        }
      }

      var finalValue = str.trim();
      return finalValue;
    }

  }

})();
