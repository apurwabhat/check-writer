(function () {
  'use strict';

  describe('service puzzlesQuizService', function () {
    var puzzlesQuizService;

    beforeEach(module('puzzlesQuizUi'));
    beforeEach(inject(function (_puzzlesQuizService_) {
      puzzlesQuizService = _puzzlesQuizService_;
    }));

    it('should be registered', function () {
      expect(puzzlesQuizService).not.toEqual(null);
    });


    describe('getCheckWriterSolution function', function () {
      it('should exist', function () {
        expect(puzzlesQuizService.getCheckWriterSolution).not.toEqual(null);
      });

      it('should return data in words', function () {
        expect(puzzlesQuizService.getCheckWriterSolution(123)).toEqual("one hundred twenty three");
        expect(puzzlesQuizService.getCheckWriterSolution(123.12)).toEqual("one hundred twenty three  and 12/100");
      });


    });

  });
})();
