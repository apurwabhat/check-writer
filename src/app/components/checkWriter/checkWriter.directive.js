(function () {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .directive('checkWriter', checkWriter);

  /** @ngInject */
  function checkWriter() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/checkWriter/checkWriter.html',
      scope: {},
      controller: CheckWriterController,
      controllerAs: 'intcheckWriter',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function CheckWriterController(puzzlesQuizService) {
      var vm = this;
      vm.showOutput = false;
      vm.submit = function () {
        vm.errorMessage = "";
        vm.showOutput = false;
        var inputVal = angular.element('#inputNumber').val();
        if (isNaN(inputVal)) {
          vm.errorMessage = "Not a valid number";
        } else {
          var getvalue = puzzlesQuizService.getCheckWriterSolution(inputVal);
          vm.showOutput = true;
          vm.myVariable = getvalue;
        }

      }
    }

  }

})();
