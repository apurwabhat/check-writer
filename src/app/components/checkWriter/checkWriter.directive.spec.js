(function () {
  'use strict';

  /**
   * @todo Complete the test
   * This example is not perfect.
   * Test should check if MomentJS have been called
   */
  describe('directive checkWriter', function () {
    var elem;
    var vm;

    beforeEach(module('puzzlesQuizUi'));
    beforeEach(inject(function ($compile, $rootScope, puzzlesQuizService, $q) {
      elem = angular.element('<check-writer></check-writer>');
      spyOn(puzzlesQuizService, 'getCheckWriterSolution').and.callFake(function () {
        return $q.when([{}, {}, {}, {}, {}, {}]);
      });
      $compile(elem)($rootScope.$new());
      $rootScope.$digest();
      vm = elem.isolateScope().intcheckWriter;

    }));

    it('should be compiled', function () {
      expect(elem.html()).not.toEqual(null);
    });

    it('should have isolate scope object with instanciate members', function () {
      expect(vm).toEqual(jasmine.any(Object));

      expect(vm.showOutput).toEqual(jasmine.any(Boolean));

    });

    it('should click on submit button', function () {
      angular.element('#clickSubmit').triggerHandler('click');
    });

  });
})();
