(function() {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {
    $log.debug('runBlock end');
  }

})();
