(function() {
  'use strict';

  angular
    .module('puzzlesQuizUi')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('quiz', {
          url: '/',
          templateUrl: 'app/quiz/main.html',
          controller: 'QuizController',
          controllerAs: 'quiz'
        })
      ;

    $urlRouterProvider.otherwise('/');
  }

})();
